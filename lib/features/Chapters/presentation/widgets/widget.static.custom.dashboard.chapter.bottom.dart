import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:muonroi/features/chapters/provider/models.chapter.template.settings.dart';
import 'package:muonroi/features/chapters/settings/settings.dart';
import 'package:muonroi/shared/settings/enums/emum.key.local.storage.dart';
import 'package:muonroi/shared/settings/enums/theme/enum.code.color.theme.dart';
import 'package:muonroi/shared/static/buttons/widget.static.button.dart';
import 'package:muonroi/features/chapters/settings/settings.dashboard.available.dart';
import 'package:muonroi/shared/settings/settings.fonts.dart';
import 'package:muonroi/core/localization/settings.language_code.vi..dart';
import 'package:muonroi/shared/settings/settings.main.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'widget.static.choose.font.color.chapter.bottom.dart';
import 'widget.static.chosse.font.family.chapter.bottom.dart';

class CustomDashboard extends StatefulWidget {
  const CustomDashboard({super.key});
  @override
  State<CustomDashboard> createState() => _CustomDashboardState();
}

class _CustomDashboardState extends State<CustomDashboard> {
  @override
  void initState() {
    _selectedRadio = KeyChapterButtonScroll.none;
    _fontSetting = FontsDefault.inter;
    _isSelected = [false, false];
    _templateAvailable =
        DashboardSettings.getDashboardAvailableSettings(context);
    _templateSettingData = TemplateSetting();
    _fontColor = themMode(context, ColorCode.textColor.name);
    _backgroundColor = themMode(context, ColorCode.modeColor.name);
    _fontSize = 15;
    _isChosseTemplate = -1;
    _initSharedPreferences();
    super.initState();
  }

  Future<void> _initSharedPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      _templateSettingData = getCurrentTemplate(_sharedPreferences, context);
      _selectedRadio =
          _templateSettingData.locationButton ?? KeyChapterButtonScroll.none;
      _fontSetting = _templateSettingData.fontFamily ?? FontsDefault.inter;
      _isSelected[_sharedPreferences.getInt('align_index') ?? 0] = true;
      _fontColor = _templateSettingData.fontColor ??
          themMode(context, ColorCode.textColor.name);
      _backgroundColor = _templateSettingData.backgroundColor ??
          themMode(context, ColorCode.modeColor.name);
      _fontSize = _templateSettingData.fontSize ?? 15;
      _isChosseTemplate = _sharedPreferences.getInt('font_chosse_index') ?? -1;
    });
  }

  late TemplateSetting _templateSettingData;
  late SharedPreferences _sharedPreferences;
  late String _fontSetting;
  late List<bool> _isSelected;
  late List<TemplateSetting> _templateAvailable;
  late KeyChapterButtonScroll _selectedRadio;
  late double _fontSize;
  late Color? _fontColor;
  late Color? _backgroundColor;
  late int _isChosseTemplate;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themMode(context, ColorCode.disableColor.name),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: themMode(context, ColorCode.disableColor.name),
        title: Title(
          color: themMode(context, ColorCode.textColor.name),
          child: Text(
            L(context, ViCode.customDashboardReadingTextInfo.toString()),
            style:
                FontsDefault.h5(context).copyWith(fontWeight: FontWeight.w700),
          ),
        ),
        leading: IconButton(
            splashRadius: 25,
            color: themMode(context, ColorCode.textColor.name),
            onPressed: () {
              Navigator.maybePop(context, true);
            },
            icon: backButtonCommon(context)),
        elevation: 0,
      ),
      body: Consumer<TemplateSetting>(
        builder: (context, templateValue, child) {
          var tempHorizontal = templateValue.isHorizontal ?? false;
          return SingleChildScrollView(
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(8.0),
                    child: Text(
                      L(context, ViCode.defaultDashboardTextInfo.toString()),
                      style: FontsDefault.h5(context)
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ),
                  // #region Chosse template
                  SizedBox(
                    height: MainSetting.getPercentageOfDevice(context,
                            expectHeight: 50)
                        .height,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: _templateAvailable.length,
                        itemBuilder: ((context, index) {
                          return Container(
                            margin:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: CircleAvatar(
                              backgroundColor: _isChosseTemplate == index
                                  ? themMode(context, ColorCode.mainColor.name)
                                  : _templateAvailable[index].backgroundColor,
                              radius: 60.0,
                              child: InkWell(
                                highlightColor:
                                    themMode(context, ColorCode.modeColor.name),
                                onTap: () {
                                  templateValue.valueSetting =
                                      _templateAvailable[index];
                                  setCurrentTemplate(_sharedPreferences,
                                      _templateAvailable[index], context);
                                  _isChosseTemplate = index;
                                  _sharedPreferences.setInt(
                                      'font_chosse_index', index);
                                },
                                child: CircleAvatar(
                                  backgroundColor:
                                      _templateAvailable[index].backgroundColor,
                                  child: Text(
                                    'AB',
                                    style: TextStyle(
                                        color: _templateAvailable[index]
                                            .fontColor),
                                  ),
                                ),
                              ),
                            ),
                          );
                        })),
                  ),
                  // #endregion
                  Container(
                    margin: const EdgeInsets.all(12.0),
                    child: Text(
                      L(context,
                          ViCode.customAnotherDashboardTextInfo.toString()),
                      style: FontsDefault.h5(context)
                          .copyWith(fontWeight: FontWeight.w700),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // #region Chosse type reading (horizontal or vertical)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              child: Row(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(right: 4.0),
                                    child: Icon(Icons.swipe_outlined,
                                        color: themMode(
                                            context, ColorCode.textColor.name)),
                                  ),
                                  Text(
                                    L(
                                        context,
                                        ViCode.scrollConfigDashboardTextInfo
                                            .toString()),
                                    style: FontsDefault.h5(context),
                                  )
                                ],
                              ),
                            ),
                            ToggleButton(
                              width: MainSetting.getPercentageOfDevice(context,
                                      expectWidth: 280)
                                  .width,
                              height: MainSetting.getPercentageOfDevice(context,
                                      expectHeight: 40)
                                  .height,
                              selectedColor:
                                  themMode(context, ColorCode.modeColor.name),
                              normalColor:
                                  themMode(context, ColorCode.textColor.name),
                              textLeft: L(
                                  context,
                                  ViCode.scrollConfigVerticalDashboardTextInfo
                                      .toString()),
                              textRight: L(
                                  context,
                                  ViCode.scrollConfigHorizontalDashboardTextInfo
                                      .toString()),
                              selectedBackgroundColor:
                                  themMode(context, ColorCode.mainColor.name),
                              noneSelectedBackgroundColor:
                                  themMode(context, ColorCode.modeColor.name),
                            ),
                          ],
                        ),
                        // #endregion
                        // #region Chosse Button scroll (hide or show)

                        !tempHorizontal
                            ? Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 12.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SizedBox(
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: const EdgeInsets.only(
                                                right: 4.0),
                                            child: Icon(
                                                Icons
                                                    .keyboard_double_arrow_down_outlined,
                                                color: themMode(context,
                                                    ColorCode.textColor.name)),
                                          ),
                                          Text(
                                            L(
                                                context,
                                                ViCode
                                                    .buttonScrollConfigDashboardTextInfo
                                                    .toString()),
                                            style: FontsDefault.h5(context),
                                          )
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                        width:
                                            MainSetting.getPercentageOfDevice(
                                                    context,
                                                    expectWidth: 280)
                                                .width,
                                        child: Row(
                                          children: [
                                            SizedBox(
                                              child: Row(
                                                children: [
                                                  Radio(
                                                    value:
                                                        KeyChapterButtonScroll
                                                            .none,
                                                    groupValue: _selectedRadio,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        var currentTemplate =
                                                            getCurrentTemplate(
                                                                _sharedPreferences,
                                                                context);

                                                        currentTemplate
                                                                .locationButton =
                                                            value;

                                                        templateValue
                                                                .valueSetting =
                                                            currentTemplate;

                                                        _selectedRadio = value ??
                                                            KeyChapterButtonScroll
                                                                .none;
                                                        setCurrentTemplate(
                                                            _sharedPreferences,
                                                            currentTemplate,
                                                            context);
                                                        templateValue
                                                                .valueSetting =
                                                            currentTemplate;
                                                      });
                                                    },
                                                  ),
                                                  Text(
                                                      L(
                                                          context,
                                                          ViCode
                                                              .buttonScrollConfigNoneDashboardTextInfo
                                                              .toString()),
                                                      style: FontsDefault.h5(
                                                          context)),
                                                ],
                                              ),
                                            ),
                                            SizedBox(
                                              child: Row(
                                                children: [
                                                  Radio(
                                                    value:
                                                        KeyChapterButtonScroll
                                                            .show,
                                                    groupValue: _selectedRadio,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        var currentTempLate =
                                                            getCurrentTemplate(
                                                                _sharedPreferences,
                                                                context);

                                                        currentTempLate
                                                                .locationButton =
                                                            value ??
                                                                KeyChapterButtonScroll
                                                                    .none;

                                                        _selectedRadio = value ??
                                                            KeyChapterButtonScroll
                                                                .none;
                                                        setCurrentTemplate(
                                                            _sharedPreferences,
                                                            currentTempLate,
                                                            context);

                                                        templateValue
                                                                .valueSetting =
                                                            currentTempLate;
                                                      });
                                                    },
                                                  ),
                                                  Text(
                                                      L(
                                                          context,
                                                          ViCode
                                                              .buttonScrollConfigDisplayDashboardTextInfo
                                                              .toString()),
                                                      style: FontsDefault.h5(
                                                          context)),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              )
                            : Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 12.0),
                              ),
                        // #endregion
                        // #region Chosse align
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              child: Row(
                                children: [
                                  Container(
                                      margin: const EdgeInsets.only(right: 4.0),
                                      child: Icon(Icons.format_align_justify,
                                          color: themMode(context,
                                              ColorCode.textColor.name))),
                                  Text(
                                    L(
                                        context,
                                        ViCode.alignConfigDashboardTextInfo
                                            .toString()),
                                    style: FontsDefault.h5(context),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              width: MainSetting.getPercentageOfDevice(context,
                                      expectWidth: 280)
                                  .width,
                              height: MainSetting.getPercentageOfDevice(context,
                                      expectHeight: 40)
                                  .height,
                              child: ToggleButtons(
                                isSelected: _isSelected,
                                selectedColor:
                                    themMode(context, ColorCode.modeColor.name),
                                color:
                                    themMode(context, ColorCode.textColor.name),
                                fillColor:
                                    themMode(context, ColorCode.mainColor.name),
                                splashColor:
                                    themMode(context, ColorCode.mainColor.name),
                                highlightColor: Colors.orange,
                                borderRadius: BorderRadius.circular(30),
                                children: [
                                  SizedBox(
                                    width: MainSetting.getPercentageOfDevice(
                                            context,
                                            expectWidth: 135)
                                        .width,
                                    height: MainSetting.getPercentageOfDevice(
                                            context,
                                            expectHeight: 20)
                                        .height,
                                    child: Text(
                                      L(
                                          context,
                                          ViCode
                                              .alignConfigLeftDashboardTextInfo
                                              .toString()),
                                      style: const TextStyle(
                                          fontFamily: FontsDefault.inter,
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  SizedBox(
                                    width: MainSetting.getPercentageOfDevice(
                                            context,
                                            expectWidth: 135)
                                        .width,
                                    height: MainSetting.getPercentageOfDevice(
                                            context,
                                            expectHeight: 20)
                                        .height,
                                    child: Text(
                                        L(
                                            context,
                                            ViCode
                                                .alignConfigRegularDashboardTextInfo
                                                .toString()),
                                        style: const TextStyle(
                                            fontFamily: FontsDefault.inter,
                                            fontWeight: FontWeight.w500),
                                        textAlign: TextAlign.center),
                                  ),
                                ],
                                onPressed: (int newIndex) {
                                  setState(() {
                                    for (int index = 0;
                                        index < _isSelected.length;
                                        index++) {
                                      if (index == newIndex) {
                                        var currentTempLate =
                                            getCurrentTemplate(
                                                _sharedPreferences, context);
                                        currentTempLate.isLeftAlign = true;
                                        _isSelected[index] = true;
                                        setCurrentTemplate(_sharedPreferences,
                                            currentTempLate, context);
                                        templateValue.valueSetting =
                                            currentTempLate;
                                        _sharedPreferences.setInt(
                                            'align_index', newIndex);
                                      } else {
                                        var currentTempLate =
                                            getCurrentTemplate(
                                                _sharedPreferences, context);
                                        currentTempLate.isLeftAlign = false;
                                        _isSelected[index] = false;
                                        setCurrentTemplate(_sharedPreferences,
                                            currentTempLate, context);
                                        templateValue.valueSetting =
                                            currentTempLate;
                                        _sharedPreferences.setInt(
                                            'align_index', newIndex);
                                      }
                                    }
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                        // #endregion
                        // #region Chosse font
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 24.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                child: Row(
                                  children: [
                                    Container(
                                        margin:
                                            const EdgeInsets.only(right: 4.0),
                                        child: Icon(Icons.text_format,
                                            color: themMode(context,
                                                ColorCode.textColor.name))),
                                    Text(
                                      L(
                                          context,
                                          ViCode.fontConfigDashboardTextInfo
                                              .toString()),
                                      style: FontsDefault.h5(context),
                                    )
                                  ],
                                ),
                              ),
                              Stack(children: [
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      color: themMode(
                                          context, ColorCode.disableColor.name),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color.fromARGB(
                                                181, 156, 154, 154),
                                            spreadRadius: 0.5)
                                      ]),
                                  width: MainSetting.getPercentageOfDevice(
                                          context,
                                          expectWidth: 280)
                                      .width,
                                  height: MainSetting.getPercentageOfDevice(
                                          context,
                                          expectHeight: 40)
                                      .height,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      templateValue.fontFamily ?? _fontSetting,
                                      style: FontsDefault.h5(context),
                                    ),
                                  ),
                                ),
                                Positioned.fill(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      borderRadius: BorderRadius.circular(20.0),
                                      onTap: () => showModalBottomSheet(
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(20),
                                          ),
                                        ),
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        context: context,
                                        builder: (context) {
                                          return const ChooseFontPage();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              ]),
                            ],
                          ),
                        ),
                        // #endregion
                        // #region Chosse font size
                        Row(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(right: 4.0),
                                  child: Icon(Icons.text_fields_outlined,
                                      color: themMode(
                                          context, ColorCode.textColor.name)),
                                ),
                                Text(
                                  L(
                                      context,
                                      ViCode.fontSizeConfigDashboardTextInfo
                                          .toString()),
                                  style: FontsDefault.h5(context),
                                ),
                              ],
                            ),
                            Expanded(
                                child: Slider(
                                    min: 10,
                                    max: 50,
                                    value: _fontSize,
                                    onChanged: (newValue) {
                                      var currentTemplate = getCurrentTemplate(
                                          _sharedPreferences, context);
                                      currentTemplate.fontSize = newValue;
                                      templateValue.valueSetting =
                                          currentTemplate;
                                      setCurrentTemplate(_sharedPreferences,
                                          currentTemplate, context);
                                      _fontSize = newValue;
                                    })),
                            GestureDetector(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (builder) {
                                        return AlertDialog(
                                            backgroundColor: themMode(context,
                                                ColorCode.modeColor.name),
                                            title: Text(
                                              L(
                                                  context,
                                                  ViCode
                                                      .limitFontSizeConfigTextInfo
                                                      .toString()),
                                              style: FontsDefault.h5(context),
                                            ),
                                            content: TextField(
                                              style: FontsDefault.h5(context),
                                              keyboardType:
                                                  TextInputType.number,
                                              inputFormatters: <TextInputFormatter>[
                                                FilteringTextInputFormatter
                                                    .digitsOnly
                                              ],
                                              onChanged: (value) {
                                                setState(() {
                                                  if (value.isNotEmpty) {
                                                    if (double.parse(value) >=
                                                            10 &&
                                                        double.parse(value) <=
                                                            50) {
                                                      var currentTemplate =
                                                          getCurrentTemplate(
                                                              _sharedPreferences,
                                                              context);
                                                      currentTemplate.fontSize =
                                                          double.parse(value);
                                                      templateValue
                                                              .valueSetting =
                                                          currentTemplate;
                                                      setCurrentTemplate(
                                                          _sharedPreferences,
                                                          currentTemplate,
                                                          context);
                                                      _fontSize =
                                                          double.parse(value);
                                                    } else {
                                                      var currentTemplate =
                                                          getCurrentTemplate(
                                                              _sharedPreferences,
                                                              context);
                                                      currentTemplate.fontSize =
                                                          15;
                                                      templateValue
                                                              .valueSetting =
                                                          currentTemplate;
                                                      setCurrentTemplate(
                                                          _sharedPreferences,
                                                          currentTemplate,
                                                          context);
                                                      _fontSize = 15;
                                                    }
                                                  }
                                                });
                                              },
                                              decoration: InputDecoration(
                                                  hintStyle:
                                                      FontsDefault.h5(context),
                                                  hintText:
                                                      '${_fontSize.ceil()}'),
                                            ));
                                      });
                                },
                                child: Text(
                                  '${_fontSize.ceil()}',
                                  style: FontsDefault.h5(context),
                                ))
                          ],
                        ),
                        // #endregion
                        // #region Chosse color (background or font)
                        Container(
                          margin: const EdgeInsets.only(bottom: 8.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          margin:
                                              const EdgeInsets.only(right: 4.0),
                                          child: Icon(Icons.color_lens_outlined,
                                              color: themMode(context,
                                                  ColorCode.textColor.name)),
                                        ),
                                        Text(
                                          L(
                                              context,
                                              ViCode
                                                  .fontColorConfigDashboardTextInfo
                                                  .toString()),
                                          style: FontsDefault.h5(context),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () => showModalBottomSheet(
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(20),
                                          ),
                                        ),
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        context: context,
                                        builder: (context) {
                                          return ChooseFontColor(
                                            colorType: KeyChapterColor.font,
                                          );
                                        },
                                      ),
                                      child: Container(
                                        margin:
                                            const EdgeInsets.only(left: 10.0),
                                        child: CircleAvatar(
                                          backgroundColor:
                                              templateValue.fontColor ??
                                                  _fontColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          margin:
                                              const EdgeInsets.only(right: 4.0),
                                          child: Icon(Icons.colorize,
                                              color: themMode(context,
                                                  ColorCode.textColor.name)),
                                        ),
                                        Text(
                                          L(
                                              context,
                                              ViCode
                                                  .backgroundConfigDashboardTextInfo
                                                  .toString()),
                                          style: FontsDefault.h5(context),
                                        ),
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () => showModalBottomSheet(
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                            top: Radius.circular(20),
                                          ),
                                        ),
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        context: context,
                                        builder: (context) {
                                          return ChooseFontColor(
                                            colorType:
                                                KeyChapterColor.background,
                                          );
                                        },
                                      ),
                                      child: Container(
                                        margin:
                                            const EdgeInsets.only(left: 10.0),
                                        child: CircleAvatar(
                                          backgroundColor:
                                              templateValue.backgroundColor ??
                                                  _backgroundColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        // #endregion
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
