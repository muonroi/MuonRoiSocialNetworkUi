enum HubStream {
  ReceiveNotificationFromFavorite,
  ReceiveGlobalNotification,
  ReceiveNotificationByUser
}
